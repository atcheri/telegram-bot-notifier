import { Container } from 'typedi';
import { Telegram } from 'telegraf';

import { chatId, token } from '../configurations/telegramConfiguration';
import { TelegramNotifier } from '../infrastructure/logger/telegramLogger';

export const DI_TYPES = Object.freeze({
  Notifier: 'Notifier',
  TelegramBot: 'TelegramBot',
  TelegramChatId: 'TelegramChatId',
  TelegramToken: 'TelegramToken'
});

Container.set(DI_TYPES.TelegramChatId, chatId);
Container.set(DI_TYPES.TelegramBot, new Telegram(token));
Container.set(
  DI_TYPES.Notifier,
  new TelegramNotifier(Container.get(DI_TYPES.TelegramBot), chatId)
);

export default Container;
