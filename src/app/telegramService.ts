import { Inject, Service, Token } from 'typedi';
import { Telegram } from 'telegraf';

import { INotifier } from 'src/domain/interfaces';

export class TelegramChatNotFoundError extends Error {}

export const TelegramNotifierService = new Token<INotifier>();

@Service('Notifier')
export class TelegramNotifier implements INotifier {
  constructor(
    @Inject('TelegramBot') private readonly _tg: Telegram,
    @Inject('TelegramChatId') private readonly _chatId: string | null
  ) {}
  send(message: string): void {
    if (!this._chatId) {
      throw new TelegramChatNotFoundError('Chat_id was not found');
    }
    this._tg.sendMessage(this._chatId, message);
  }
}
