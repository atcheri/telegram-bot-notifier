const { BOT_TOKEN: token = '', CHAT_ID: chatId = null } = process.env;

export { token, chatId };
