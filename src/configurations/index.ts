const whiteIpsEnv = process.env.WHITE_IPS;

const buildWhitelist = (): string[] => {
  const defaultWhiteListIps = ['localhost'];
  if (whiteIpsEnv) {
    return defaultWhiteListIps.concat(whiteIpsEnv.split(',').map(ip => ip.trim()));
  }
  return defaultWhiteListIps;
};

export const whitelist = buildWhitelist();
