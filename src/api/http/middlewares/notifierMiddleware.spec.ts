import httpMocks from 'node-mocks-http';

import { INotifier } from 'src/domain/interfaces';
import NotifierMiddleware from './notifierMiddleware';

const doesNothing = () => {
  return;
};

describe('NotifierMiddleware', () => {
  describe('Given a notifier failing on send function call', () => {
    const notifier: INotifier = {
      send: (message: string) => {
        throw Error('Test telegram send message error');
      }
    };
    it('[Error] throws an Error', () => {
      const request = httpMocks.createRequest({
        method: 'POST',
        url: '/webhook',
        headers: {
          'Content-Type': 'application/json'
        },
        body: {
          message: 'Non empty message'
        }
      });
      const middleware = new NotifierMiddleware(notifier);
      expect(middleware.use(request, null, doesNothing)).rejects.toEqual(
        Error('Test telegram send message error')
      );
    });
  });

  describe('Given a notifier failing on send function call', () => {
    const sendSpy = jest.fn();
    const nextSpy = jest.fn();
    const notifier: INotifier = {
      send: (message: string) => {
        sendSpy(message);
      }
    };
    beforeEach(() => {
      sendSpy.mockReset();
      nextSpy.mockReset();
    });

    it("[No Error] doesn't call the notifier send function if message is empty", (done) => {
      const request = httpMocks.createRequest({
        method: 'POST',
        url: '/webhook',
        headers: {
          'Content-Type': 'application/json'
        },
        body: {
          message: ''
        }
      });
      const middleware = new NotifierMiddleware(notifier);
      middleware.use(request, null, nextSpy).then(() => {
        expect(sendSpy).not.toHaveBeenCalled();
        expect(nextSpy).toHaveBeenCalledTimes(1);
        done();
      });
    });

    it('[No Error] calls the send message if a non empty message is provided', (done) => {
      const request = httpMocks.createRequest({
        method: 'POST',
        url: '/webhook',
        headers: {
          'Content-Type': 'application/json'
        },
        body: {
          message: 'Non empty message'
        }
      });
      const middleware = new NotifierMiddleware(notifier);
      middleware.use(request, null, nextSpy).then(() => {
        expect(sendSpy).toHaveBeenCalledTimes(1);
        expect(nextSpy).toHaveBeenCalledTimes(1);
        done();
      });
    });
  });
});
