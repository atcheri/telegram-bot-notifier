import { Request } from 'express';
import { ExpressMiddlewareInterface } from 'routing-controllers';
import { INotifier } from 'src/domain/interfaces';
import { Inject } from 'typedi';

class NotifierMiddlewareError extends Error {}

class NotifierMiddleware implements ExpressMiddlewareInterface {
  constructor(@Inject('Notifier') private readonly _notifier: INotifier) {}
  async use(request: Request, _: any, next: (err?: any) => any): Promise<any> {
    try {
      const { message } = await request.body;
      if (message) {
        this._notifier.send(message);
      }
      next();
    } catch (e) {
      throw new NotifierMiddlewareError(e.message);
    }
  }
}

export default NotifierMiddleware;
