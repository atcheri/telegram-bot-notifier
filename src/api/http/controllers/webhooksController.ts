import {
  BodyParam,
  Get,
  JsonController,
  Post,
  Req,
  UseAfter
} from 'routing-controllers';

import NotifierMiddleware from '../infrastructure/logger/telegramLogger';

@JsonController('/webhook')
export class WebhookController {
  @Get()
  heartbeat(): boolean {
    return true;
  }

  @UseAfter(NotifierMiddleware)
  @Post()
  broadcast(@Req() req: any, @BodyParam('message') message: string): string {
    return message;
  }
}
