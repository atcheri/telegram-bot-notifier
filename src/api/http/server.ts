import dotenv from 'dotenv';
dotenv.config();
import { createExpressServer, useContainer } from 'routing-controllers';

import { whitelist } from 'src/configurations';
import container from 'src/ioc/container';
import { WebhookController } from './controllers/webhooksController';

const run = (): void => {
  const corsOptions = {
    origin: (
      origin: string | undefined,
      callback: (err: Error | null, allow?: boolean) => void
    ): void => {
      if (!origin || whitelist.indexOf(origin) !== -1) {
        callback(null, true);
      } else {
        callback(new Error('Not in white list'));
      }
    }
  };
  useContainer(container);
  const app = createExpressServer({
    cors: corsOptions,
    controllers: [WebhookController]
    // classTransformer: true
  });
  app.listen(8000, () => {
    console.log('Server Started at Port, 8000');
  });
};

export default {
  run
};
