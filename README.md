# Telegram bot notifier (typesript)

### Installation
```script
yarn
```
or
```script
npm install
```
### Run in dev mode
```script
yarn dev
```
or with debug
```script
yarn debug
```

### Run in prod mode
```script
yarn start
```

## How to use
1. Set 2 environmental variables:
    1. BOT_TOKEN: that corresponds to the telegram bot token [(how to create one)](https://core.telegram.org/bots#creating-a-new-bot)
    2. CHAT_ID: that corresponds to the chat ID you want your BOT to send a message to [(see how to get the chat ID in this Stackoverflow thread)](https://stackoverflow.com/a/46247058)
2. Use a tool to execute an HTTP:
example with curl:
```script
curl -X POST -H "Content-Type: application/json" http://localhost:8000/webhook -d '{"message": "Write your message here"}'
```
You should get your message as a response and the message sent to your chat
